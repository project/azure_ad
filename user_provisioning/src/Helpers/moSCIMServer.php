<?php

namespace Drupal\user_provisioning\Helpers;

use Drupal\Core\Render\Markup;

/**
 *
 */
class moSCIMServer {
  private $url_path;
  private $base_url;

  /**
   * Constructor for the Provider Specific Provisioning tab.
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->url_path = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('user_provisioning') . '/images';
  }

  /**
   * @return array of providers for provider specific provisioning
   */
  public function providerList($custom = FALSE): array {
    if ($custom == 'custom') {
      return [
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Custom App" src="' . $this->url_path . '/customapp.png" ><br><strong>Custom App</strong> '),
      ];
    }
    else {
      return [
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Custom server" src="' . $this->url_path . '/customapp.png" ><br><strong>Custom Server</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="AWS SSO" src="' . $this->url_path . '/AWS.png" ><br><strong>AWS SSO</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Drupal" src="' . $this->url_path . '/Drupal.png" ><br><strong>Drupal</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Okta" src="' . $this->url_path . '/okta.png"><br><strong>Okta</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Azure" src="' . $this->url_path . '/azure.png" ><br><strong>Azure</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="OneLogin" src="' . $this->url_path . '/onelogin.png" ><br><strong>OneLogin</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Google" src="' . $this->url_path . '/google.png" ><br><strong>Google Apps</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="JumpCloud" src="' . $this->url_path . '/jumpcloud.png" ><br><strong>JumpCloud</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Centrify" src="' . $this->url_path . '/centrify.png" ><br><strong>Centrify</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="PingOne" src="' . $this->url_path . '/pingone.png" ><br><strong>PingOne</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Miniornage" src="' . $this->url_path . '/miniorange.png" ><br><strong>miniOrange</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="CyberArk" src="' . $this->url_path . '/cyberark.png" ><br><strong>Cyberark</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Wordpress" src="' . $this->url_path . '/wordpress.png"><br><strong>Wordpress</strong> '),
        Markup::create('<img class="mo_user_provisioning_img_logo" alt="Joomla" src="' . $this->url_path . '/Joomla.png" ><br><strong>Joomla</strong> '),
      ];
    }
  }

}
