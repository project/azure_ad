<?php

namespace Drupal\user_provisioning;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\user\Entity\User;

/**
 *
 */
class moUserProvisioningUtilities {

  /**
   *
   */
  public static function customUserFields(): array {
    $custom_fields = [];
    $usr = User::load(\Drupal::currentUser()->id());
    $usrVal = $usr->toArray();
    foreach ($usrVal as $key => $value) {
      $custom_fields[$key] = $key;
    }
    return $custom_fields;
  }

  /**
   *
   */
  public static function mo_get_drupal_core_version() {
    return explode('.', \DRUPAL::VERSION)[0];
  }

  /**
   *
   */
  public static function userProvisioningConfigGuide(array &$form, FormStateInterface $form_state) {

    $form['miniorange_user_provisioning_guide'] = [
      '#markup' => '<div class="mo_user_provisioning_table_layout_setup_guide mo_user_provisioning_container_2" id="mo_guide_vt">
                              <div style="font-size: 15px;">To see detailed documentation of how to configure Drupal User Provisioning Module</div></br>',
    ];

    if (\Drupal::request()->get('tab_name') == moUserProvisioningConstants::SCIM_SERVER_TAB_NAME) {
      $mo_server_azure = Markup::create('<strong><a href="' . moUserProvisioningConstants::AZURE_GUIDE . '" class="mo_guide_text-color" target="_blank">Azure AD</a></strong>');
      $mo_server_okta = Markup::create('<strong><a href="' . moUserProvisioningConstants::OKTA_GUIDE . '" class="mo_guide_text-color" target="_blank">Okta</a></strong>');
      $mo_server_onelogin = Markup::create('<strong><a href="' . moUserProvisioningConstants::ONELOGIN_GUIDE . '" class="mo_guide_text-color" target="_blank">OneLogin</a></strong>');
      $mo_server_google = Markup::create('<strong><a href="' . moUserProvisioningConstants::GOOGLE_GUIDE . '" class="mo_guide_text-color" target="_blank">Google Apps</a></strong>');
      $mo_server_miniorange = Markup::create('<strong><a href="' . moUserProvisioningConstants::MINIORANGE_GUIDE . '" class="mo_guide_text-color" target="_blank">miniOrange</a></strong>');
      $mo_server_jumpcloud = Markup::create('<strong><a href="' . moUserProvisioningConstants::JUMPCLOUD_GUIDE . '" class="mo_guide_text-color" target="_blank">JumpCloud</a></strong>');
      $mo_server_centrify = Markup::create('<strong><a href="' . moUserProvisioningConstants::CENTRIFY_GUIDE . '" class="mo_guide_text-color" target="_blank">Centrify</a></strong>');
      $mo_server_pingone = Markup::create('<strong><a href="' . moUserProvisioningConstants::PINGONE_GUIDE . '" class="mo_guide_text-color" target="_blank">PingOne</a></strong>');
      $mo_server_cyberark = Markup::create('<strong><a href="' . moUserProvisioningConstants::CYBERARK_GUIDE . '" class="mo_guide_text-color" target="_blank">CyberArk</a></strong>');
      $mo_Drupal = Markup::create('<strong><a href="' . moUserProvisioningConstants::DRUPAL_GUIDE . '" class="mo_guide_text-color" target="_blank">Drupal</a></strong>');

      $mo_table_content = [
            [$mo_server_azure, $mo_server_okta],
            [$mo_server_onelogin, $mo_server_google],
            [$mo_server_miniorange, $mo_server_jumpcloud],
            [$mo_server_centrify, $mo_server_pingone],
            [$mo_server_cyberark, $mo_Drupal],
      ];

      $header_data = t('SCIM Server Setup Guides');

    }
    elseif (\Drupal::request()->get('tab_name') == moUserProvisioningConstants::PROVIDER_SPECIFIC_PROVISIONING_TAB_NAME) {

      $mo_azure_ad_pro_specific = Markup::create('<strong><a href="' . moUserProvisioningConstants::AZURE_PRO_SPECIFIC_GUIDE . '" class="mo_guide_text-color" target="_blank">Azure</a></strong>');
      $mo_okta_pro_specific = Markup::create('<strong><a href="' . moUserProvisioningConstants::OKTA_PRO_SPECIFIC_GUIDE . '" class="mo_guide_text-color" target="_blank">Okta</a></strong>');

      $mo_table_content = [
            [$mo_azure_ad_pro_specific, $mo_okta_pro_specific],
      ];

      $header_data = t('Provider Specific Provisioning Setup Guides');

    }
    else {
      $mo_AWS_SSO = Markup::create('<strong><a href="' . moUserProvisioningConstants::AWS_SSO_GUIDE . '" class="mo_guide_text-color" target="_blank">AWS SSO</a></strong>');
      $mo_Wordpress = Markup::create('<strong><a href="' . moUserProvisioningConstants::WORDPRESS_GUIDE . '" class="mo_guide_text-color" target="_blank">Wordpress</a></strong>');
      $mo_Drupal = Markup::create('<strong><a href="' . moUserProvisioningConstants::DRUPAL_GUIDE . '" class="mo_guide_text-color" target="_blank">Drupal</a></strong>');
      $mo_Joomla = Markup::create('<strong><a href="' . moUserProvisioningConstants::JOOMLA_GUIDE . '" class="mo_guide_text-color" target="_blank">Joomla</a></strong>');

      $mo_table_content = [
            [$mo_AWS_SSO, $mo_Wordpress],
            [$mo_Drupal, $mo_Joomla],
      ];

      $header_data = t('SCIM Client Setup Guides');
    }

    $header = [[
      'data' => $header_data,
      'colspan' => 2,
    ],
    ];

    $form['modules'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $mo_table_content,
      '#attributes' => ['class' => ['setup_guides']],
      '#responsive' => TRUE,
    ];
    $form['miniorange_user_provisioning_guide_end'] = [
      '#markup' => '</div>',
    ];
  }

  /**
   *
   */
  public static function moProvShowCustomerSupportIcon(array &$form, FormStateInterface $form_state) {
    global $base_url;
    $support_image_path = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('user_provisioning') . '/images';

    $form['mo_user_provisioning_customer_support_icon'] = [
      '#markup' => t('<a class="use-ajax mo-bottom-corner" href="CustomerSupportProv"><img src="' . $support_image_path . '/mo-customer-support.png" alt="Support Icon"></a>'),
    ];
  }

}
