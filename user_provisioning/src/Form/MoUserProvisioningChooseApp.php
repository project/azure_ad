<?php

namespace Drupal\user_provisioning\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 *
 */
class MoUserProvisioningChooseApp extends FormBase {


  private $base_url;
  private string $url_path;

  /**
   *
   */
  public function __construct() {
    global $base_url;
    $this->base_url = $base_url;
    $this->url_path = $this->base_url . '/' . \Drupal::service('extension.list.module')->getPath('user_provisioning') . '/images';
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'mo_provisioning_choose_app';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $app_name = NULL) {
    $form['#prefix'] = '<div id="modal_example_form">';
    $form['#suffix'] = '</div>';
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $app = $this->appDisplayName($app_name);

    $rows = [
          [Markup::create('<div class="mo_azure_step_1_img mo_user_provisioning_text_center"><h4>Drupal to ' . $app . ' Sync</h4>'), Markup::create('<div class="mo_azure_step_1_img mo_user_provisioning_text_center"><h4>' . $app . ' to Drupal Sync</h4>')],
          [Markup::create('<a href="?app_name=' . $app_name . '"><img class="mo_azure_sync_gif" src="' . $this->url_path . '/drupal_to_scim.gif" alt="Drupal to SCIM Sync"></div></a>'), Markup::create('<a href="?app_name=scim_server"><img class="mo_azure_sync_gif" src="' . $this->url_path . '/scim_to_drupal.gif" alt="SCIM to Drupal Sync"></div></a>')],
    ];

    $form['azure_ad_step1_how_to_sync'] = [
      '#type' => 'table',
      '#responsive' => TRUE,
      '#rows' => $rows,
      '#attributes' => ['style' => 'border-collapse: separate;', 'class' => ['how_to_perform_sync']],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }

  /**
   *
   */
  private function appDisplayName($app_name) {

    $appDisplay = [
      'aws_sso' => 'AWS SSO',
      'azure_ad' => 'Azure AD',
      'okta' => 'Okta',
      'onelogin' => 'OneLogin',
      'google' => 'Google Apps',
      'cyberark' => 'CyberArk',
      'miniorange' => 'miniOrange',
      'jumpcloud' => 'JumpCloud',
      'ping' => 'PingOne',
      'centrify' => 'Centrify',
      'salesforce' => 'Salesforce',
      'oracle' => 'Oracle',
      'drupal' => 'Drupal',
      'joomla' => 'Joomla',
      'wordpress' => 'WordPress',
      'alexis' => 'AlexisHR Provisioning',
      'anaplan' => 'Anaplan SCIM API',
      'blink' => 'Blink',
      'calendly' => 'Calendly',
      'circlehd' => 'CircleHD',
      'curity' => 'Curity Identity Server',
      'federated_dir' => 'Federated Directory',
      'fusionauth' => 'FusionAuth',
      'github' => 'GitHub Business',
      'idaas' => 'idaas.nl',
      'monokee' => 'Monokee',
      'netiq' => 'NetIQ Identity Manager',
      'workday' => 'Workday Peakon',
      'reward' => 'Reward Gateway',
      'sailpoint' => 'SailPoint',
      'sap' => 'SAP',
      'trello' => 'Trello',
      'wso2' => 'WSO2 Charon',
      'custom' => 'Custom Provider',
    ];

    return $appDisplay[$app_name];
  }

}
