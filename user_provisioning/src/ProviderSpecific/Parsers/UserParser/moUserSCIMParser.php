<?php

namespace Drupal\user_provisioning\ProviderSpecific\Parsers\UserParser;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user_provisioning\Helpers\moUserProvisioningLogger;
use Drupal\user_provisioning\ProviderSpecific\Parsers\moResourceParserInterface;

/**
 *
 */
class moUserSCIMParser implements moResourceParserInterface {
  private moUserProvisioningLogger $mo_logger;
  private ImmutableConfig $config;

  /**
   *
   */
  public function __construct() {
    $this->mo_logger = new moUserProvisioningLogger();
    $this->config = \Drupal::config('user_provisioning.settings');
  }

  /**
   * {@inheritDoc}
   */
  public function search($resource_id): string {
    $this->mo_logger->addLog("Creating search request parameter.", __LINE__, __FUNCTION__, __FILE__);
    return 'Users?filter=userName eq "' . $resource_id . '"';
  }

  /**
   * {@inheritDoc}
   */
  public function get($resource_id) {
    // @todo Implement get() method.
  }

  /**
   * {@inheritDoc}
   */
  public function post(EntityInterface $entity) {
    // The bare minimum attributes.
    $post_request_body = $this->getDefaultFields($entity, $this->config->get('mo_user_provisioning_app_name'));
    $this->mo_logger->addFormattedLog($post_request_body, __LINE__, __FUNCTION__, __FILE__, 'User create request body: ');

    // Additional attributes
    // consider the mapping made before picking up the additional attributes.
    return $post_request_body;
  }

  /**
   * {@inheritDoc}
   */
  public function put(array $resource) {
    // @todo Implement put() method.
  }

  /**
   * {@inheritDoc}
   */
  public function patch(array $resource) {
    // @todo Implement patch() method.
  }

  /**
   * {@inheritDoc}
   */
  public function delete(array $resource) {
    // @todo Implement delete() method.
  }

  /**
   * Provides the body of the POST request for user.
   *
   * It generates an array containing the default fields of a user provided in the Drupal. It includes following:-
   *
   * -uuid
   *
   * -Username
   *
   * -Email
   *
   * -Preferred Language
   *
   * -Timezone
   *
   * -Status
   *
   * In case of the configured application is aws sso, below additional attributes are mandatory
   *
   * -first name
   *
   * -last name
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $app_name
   *   Name of the configured app.
   *
   * @return array
   */
  public function getDefaultFields(EntityInterface $entity, string $app_name): array {
    $mapping_array = json_decode($this->config->get('mo_user_prov_mapping_conf'), TRUE);

    $fields = [
      'externalId' => $entity->uuid(),
      'userName' => $entity->getDisplayName(),
      "emails" => [
              [
                "value" => $entity->getEmail(),
                "type" => "work",
                "primary" => TRUE,
              ],
      ],
      'displayName' => $entity->getDisplayName(),
      'preferredLanguage' => $entity->get('preferred_langcode')->value,
      'timezone' => $entity->get('timezone')->value,
      'active' => (bool) $entity->get('status')->value,
      'name' => [
        'familyName' => $entity->get($this->getDrupalAttr($mapping_array, 'name.familyName'))->value,
        'givenName' => $entity->get($this->getDrupalAttr($mapping_array, 'name.givenName'))->value,
      ],
    ];
    return $fields;
  }

  /**
   *
   */
  private function getDrupalAttr($array, $scim_attr) {
    foreach ($array as $item) {
      if ($item['scim_attr'] === $scim_attr) {
        return $item['drupal_attr'];
      }
    }
  }

  /**
   *
   */
  public function deactivate(EntityInterface $entity) {
    // @todo Implement deactivate() method.
  }

}
