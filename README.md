# Azure Sync And Provisioning 

The miniOrange Azure Sync module offers bidirectional user synchronization between your Azure AD/B2C and your Drupal site using Microsoft Graph APIs. Make sure to check out the list of [supported features and integrations](https://plugins.miniorange.com/drupal-azure-ad-sync) to increase the functionality of your Drupal site.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/azure_ad).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/azure_ad).

## Table of contents

- Installation
- Configuration
- Support

## Installation

Install as you would normally install a contributed Drupal module. For further information, see
[Installing Azure Sync and Provisioning Module](https://www.drupal.org/docs/contributed-modules/azure-drupal-sync/installation-of-drupal-azure-sync-and-provisioning-module).

## Configuration

You can follow the following mentioned link for a step-by-step [Setup Guide](https://www.drupal.org/docs/contributed-modules/azure-drupal-sync/setup-drupal-azure-synchronization).

## Support

If you require any User Provisioning / Synchronization or need any help with installing or configuring this module, please feel free to reach out to us on our 24*7 support at [drupalsupport@xecurify.com](mailto:drupalsupport@xecurify.com).