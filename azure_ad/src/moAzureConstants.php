<?php

namespace Drupal\azure_ad;

class moAzureConstants {
    // Endpoints.
    const AUTHORIZE_ENDPOINT = 'https://login.microsoftonline.com/{tenant}/oauth2/v2.0/authorize';
    const TOKEN_ENDPOINT = 'https://login.microsoftonline.com/{tenant}/oauth2/v2.0/token';
    const USERINFO_ENDPOINT = 'https://graph.microsoft.com/beta/users/';
    const CHECK_USER = 'https://graph.microsoft.com/v1.0/users?$count=true&$filter=startsWith(userPrincipalName,' . "'resource_id'" . ')&$select=id,userPrincipalName';

    // Scope.
    const SCOPE = "https://graph.microsoft.com/.default";
    const SUPPORT_QUERY_CONTENT = "?subject=Drupal Azure AD User Sync Module: Licensing Query&body=I want to enquire regarding the pricing of Drupal Azure Sync module";

}
