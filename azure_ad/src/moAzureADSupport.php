<?php

namespace Drupal\azure_ad;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\user_provisioning\moUserProvisioningCustomer;
use Drupal\user_provisioning\moUserProvisioningSupport;
use Drupal\user_provisioning\moUserProvisioningUtilities;

class moAzureADSupport {
    private ImmutableConfig $config;
    public $email;
    public $phone;
    public $query;
    public $query_type;

    public function __construct($email, $phone, string $query, string $query_type) {
        $this->email = $email;
        $this->phone = $phone;
        $this->query = $query;
        $this->query_type = $query_type;
        $this->config = \Drupal::config('user_provisioning.settings');
    }

    public function sendSupportQuery() {
        $modules_info = \Drupal::service('extension.list.module')->getExtensionInfo('azure_ad');
        $modules_version = $modules_info['version'];

        if ($this->query_type == 'Trial Request' || $this->query_type == 'Contact Support') {

            $url = moUserProvisioningConstants::MO_NOTIFY_SEND;
            $request_for = $this->query_type == 'Trial Request' ? 'Trial' : 'Support';
            $subject = $request_for . ' request for Drupal-' . \Drupal::VERSION . ' Azure AD Sync Module | ' . $modules_version;
            $this->query = ($this->query_type == 'Contact Support' ? 'Query - ' : $request_for . ' requested for - ') . $this->query;

            $utilities = new MoAzureUtilities();
            [$customerKey, $currentTimeInMillis, $hashValue] = $utilities->CustomerApiKey();

            $phone = $this->query_type == 'Contact Support' ? '<strong>Support needed for:</strong> ' : 'Phone Number: ';
            $content = '<div >Hello, <br><br>Company :<a href="' . $_SERVER['SERVER_NAME'] . '" target="_blank" >' . $_SERVER['SERVER_NAME'] . '</a><br><br>' . $phone . $this->phone . '<br><br>Email:<a href="mailto:' . $this->email . '" target="_blank">' . $this->email . '</a><br><br>Query:[DRUPAL ' . moUserProvisioningUtilities::mo_get_drupal_core_version() . ' Azure AD Sync Free | PHP ' . phpversion() . ' | ' . $modules_version . ' ] ' . $this->query . '</div>';

            $fields = [
              'customerKey' => $customerKey,
              'sendEmail' => TRUE,
              'email' => [
                'customerKey' => $customerKey,
                'fromEmail' => $this->email,
                'fromName' => 'miniOrange',
                'toEmail' => moUserProvisioningConstants::SUPPORT_EMAIL,
                'toName' => moUserProvisioningConstants::SUPPORT_EMAIL,
                'subject' => $subject,
                'content' => $content,
              ],
            ];

            $header = [
              'Content-Type' => 'application/json',
              'Customer-Key' => $customerKey,
              'Timestamp' => $currentTimeInMillis,
              'Authorization' => $hashValue,
            ];

        }
        else {

            $this->query = '[Drupal ' . \Drupal::VERSION . ' Azure AD Sync Module | PHP ' . phpversion() . ' | ' . $modules_version . '] ' . $this->query;
            $fields = [
              'company' => $_SERVER['SERVER_NAME'],
              'email' => $this->email,
              'phone' => $this->phone,
              'ccEmail' => moUserProvisioningConstants::SUPPORT_EMAIL,
              'query' => $this->query,
            ];

            $url = moUserProvisioningConstants::CONTACT_US;
            $header = [
              'Content-Type' => 'application/json',
              'charset' => 'UTF-8',
              'Authorization' => 'Basic',
            ];
        }

        $field_string = json_encode($fields);
        $mo_user_provisioning_customer = new moUserProvisioningCustomer(NULL, NULL, NULL, NULL);
        $response = $mo_user_provisioning_customer->callService($url, $field_string, $header);
        return TRUE;
    }

}
