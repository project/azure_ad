<?php

namespace Drupal\azure_ad\Controller;

use Drupal;
use Drupal\azure_ad\MoAzureUtilities;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\user_provisioning\moUserProvisioningUtilities;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MoAzureController extends ControllerBase
{
    private Config $config_factory;
    protected $messenger;
    protected $formBuilder;
    private ImmutableConfig $config;
    private ParameterBag $get;

    public function __construct() {
        $this->config_factory = \Drupal::configFactory()->getEditable('azure_ad.settings');
        $this->config = \Drupal::config('user_provisioning.settings');
        $this->messenger = Drupal::messenger();
        $this->formBuilder = Drupal::formBuilder();
        $this->get= \Drupal::request()->query;
    }

    public function openTrialRequestForm(): AjaxResponse
    {
        $response = new AjaxResponse();
        $modal_form = $this->formBuilder->getForm('\Drupal\azure_ad\Form\MoAzureRequestTrial');
        $response->addCommand(new OpenModalDialogCommand('Request 7-Days Full Feature Trial License', $modal_form, ['width' => '45%'] ) );
        return $response;
    }

    public function openCustomerRequestForm(): AjaxResponse
    {
        $response = new AjaxResponse();
        $modal_form = $this->formBuilder->getForm('\Drupal\azure_ad\Form\MoAzureCustomerRequest');
        $response->addCommand(new OpenModalDialogCommand('Contact miniOrange Support', $modal_form, ['width' => '45%'] ) );
        return $response;
    }

    public function openconfigform(): AjaxResponse
    {
        $response = new AjaxResponse();
        $modal_form =  $this->formBuilder->getForm('\Drupal\azure_ad\Form\MoReviewAzureConfig');
        $response->addCommand(new OpenModalDialogCommand('Azure Configurations', $modal_form, ['width' => '60%'] ) );
        return $response;
    }

    public function configureApp($app){

        $this->config_factory->set('azure_ad_provisioning_step', 'step3')->save();
        $path = Url::fromRoute('azure_ad.overview')->toString();
        $response = new RedirectResponse($path);
        $response->send();
        return $response;
    }

}
