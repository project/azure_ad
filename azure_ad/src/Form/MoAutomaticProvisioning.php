<?php

namespace Drupal\azure_ad\Form;

use Drupal;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class MoAutomaticProvisioning extends FormBase{

    private ImmutableConfig $config;
    protected $messenger;
    private Config $config_factory;

    public function __construct()
    {
        $this->config = Drupal::config('azure_ad.settings');
        $this->messenger = Drupal::messenger();
        $this->config_factory = \Drupal::configFactory()->getEditable('azure_ad.settings');
    }

    public function getFormId() {
        return 'mo_azure_review_config';
    }


    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['#prefix'] = '<div id="modal_support_form">';
        $form['#suffix'] = '</div>';
        $form['status_messages'] = [
            '#type' => 'status_messages',
            '#weight' => -10,
        ];

        $form['mo_azure_ad_to_drupal_library'] = [
            '#attached' => [
                'library' => [
                    'azure_ad/azure_ad.admin',
                ]
            ],
        ];

        $form['azure_ad_to_drupal_header_style'] = [
            '#markup' => t('<div class="mo_azure_header_container_step10">'),
        ];

        $form['mo_azure_user_operations'] =[
            '#type' => 'fieldset',
            '#title' => t('Automatic Provisioning Configurations'),
        ];

        $form['mo_azure_user_operations']['azure_ad_automatic_user_operations'] = [
            '#type' => 'table',
            '#responsive' => TRUE,
            '#attributes' => ['class' => ['mo_azure_provisioning_types_ajax']],
        ];

        $row['read_user_azure_ad_manual'] = [
            '#type' => 'checkbox',
            '#title' => t('Read user'),
            '#default_value' => 1,
            '#disabled' => true,
        ];

        $row['create_user_azure_ad_manual'] = [
            '#type' => 'checkbox',
            '#title' => t('Create user'),
            '#default_value' => $this->config->get('azure_ad_automatic_provisioning_checkbox'),
        ];

        $row['update_user_azure_ad'] = [
            '#type' => 'checkbox',
            '#title' => t('Update user <a href="azure_upgrade_plans"><b><small>[PREMIUM]</small></b></a>'),
            '#disabled' => TRUE,
        ];

        $row['deactivate_user_azure_ad'] = [
            '#type' => 'checkbox',
            '#title' => t('Deactivate user <a href="azure_upgrade_plans"><b><small>[PREMIUM]</small></b></a>'),
            '#disabled' => TRUE,
        ];

        $row['delete_user_azure_ad'] = [
            '#type' => 'checkbox',
            '#title' => t('Delete user <a href="azure_upgrade_plans"><b><small>[PREMIUM]</small></b></a>'),
            '#disabled' => TRUE,
        ];

        $form['mo_azure_user_operations']['azure_ad_automatic_user_operations']['operations'] = $row;

        $form['actions'] = array('#type' => 'actions');
        $form['actions']['send'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save'),
          '#attributes' => [
            'class' => [
              'use-ajax',
              'button--primary'
            ],
          ],
          '#ajax' => [
            'callback' => [$this,'submitModalFormAjax'],
            'event' => 'click',
          ],
        ];

        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
        return $form;
    }

    public function submitModalFormAjax(array $form, FormStateInterface $form_state) {

        $response = new AjaxResponse();
        // If there are any form errors, AJAX replace the form.
        if ( $form_state->hasAnyErrors() ) {
            $response->addCommand(new ReplaceCommand('#modal_support_form', $form));
        } else {
            $form_values= $form_state->getValues();
            $auto_create_user = $form_values['azure_ad_automatic_user_operations']['operations']['create_user_azure_ad_manual'];

            $this->config_factory->set('azure_ad_automatic_provisioning_checkbox', $auto_create_user)->save();
            $this->messenger->addstatus(t('Configurations saved successfully.'));
    
            $response->addCommand(new RedirectCommand(Url::fromRoute('azure_ad.overview')->toString()));
        }
        return $response;
    }

    public function validateForm(array &$form, FormStateInterface $form_state) { }

    public function submitForm(array &$form, FormStateInterface $form_state) { }

}