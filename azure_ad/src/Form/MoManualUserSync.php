<?php 

namespace Drupal\azure_ad\Form;

use Drupal;
use Drupal\azure_ad\Form\MoDrupalToAzureSync;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Exception;

class MoManualUserSync extends FormBase
{
    protected $messenger;

    public function __construct()
    {
        $this->messenger = Drupal::messenger();
    }

    public function getFormId() {
        return 'mo_azure_request_support';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {
        $form['#prefix'] = '<div id="modal_support_form">';
        $form['#suffix'] = '</div>';
        $form['status_messages'] = [
            '#type' => 'status_messages',
            '#weight' => -10,
        ];

        $form['markup_library'] = array(
          '#attached' => array(
            'library' => array(
              "azure_ad/azure_ad.admin",
              'core/drupal.dialog.ajax',
            )
          ),
        );

         $form['mo_azure_ad_manual_provisioning_fieldset']['manual_provisioning_choose_operations']['create_user_fieldset']=[
            '#type' => 'fieldset',
            '#title' => $this->t('Sync Users'),
        ];

        $form['mo_azure_ad_manual_provisioning_fieldset']['manual_provisioning_choose_operations']['create_user_fieldset']['mo_azure_drupal_username'] = [
            '#type' => 'textfield',
            '#attributes' => ['placeholder' => 'Search Drupal username of user to sync'],
            '#autocomplete_route_name' => 'user_provisioning.autocomplete',
            '#required' => true,
            '#prefix' => '<p class="mo_azure_highlight_background"><strong>Note:</strong> Search the username of user in Drupal to sync (create) it to the Active Directory.</p><div class="container-inline">',
        ];


        $form['mo_azure_ad_manual_provisioning_fieldset']['manual_provisioning_choose_operations']['create_user_fieldset']['mo_azure_sync_button'] = [
            '#type' => 'submit',
            '#value' => t('Sync'),
            '#button_type' => 'primary',
            '#attributes' => [
                'class' => [
                  'use-ajax',
                  'button--primary',
                  'sync_button'
                ],
              ],
              '#ajax' => [
                'callback' => [$this,'moAzureManualSync'],
                'event' => 'click',
              ],
        ];

        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
        return $form;
    }

    public function submitModalFormAjax(array $form, FormStateInterface $form_state) {

    }

    public function validateForm(array &$form, FormStateInterface $form_state) {
    }

    public function submitForm(array &$form, FormStateInterface $form_state) { }


    public function moAzureManualSync(array &$form, FormStateInterface $form_state){
        $response = new AjaxResponse();
       
       // If there are any form errors, AJAX replace the form.
       if ( $form_state->hasAnyErrors() ) {
           $response->addCommand(new ReplaceCommand('#modal_support_form', $form));
       } else {
           $sync = new MoDrupalToAzureSync();
           $sync->moAzureManualSync($form,$form_state);
           $response->addCommand(new ReplaceCommand('#modal_support_form', $form));
       }
       return $response;
   }


}
