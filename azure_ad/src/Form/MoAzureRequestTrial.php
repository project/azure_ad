<?php

namespace Drupal\azure_ad\Form;

use Drupal;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\azure_ad\MoAzureUtilities;
use Drupal\user_provisioning\moUserProvisioningSupport;

class MoAzureRequestTrial extends FormBase
{
    private ImmutableConfig $config;
    protected $messenger;

    public function __construct()
    {
        $this->config = Drupal::config('user_provisioning.settings');
        $this->messenger = Drupal::messenger();
    }

    public function getFormId() {
        return 'mo_azure_request_trial';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {

        $form['#prefix'] = '<div id="modal_example_form">';
        $form['#suffix'] = '</div>';
        $form['status_messages'] = [
            '#type' => 'status_messages',
            '#weight' => -10,
        ];

        $user_email = $this->config->get('user_provisioning_customer_admin_email');

        $form['mo_azure_ad_trial_email_address'] = [
            '#type' => 'email',
            '#title' => t('Email'),
            '#default_value' => $user_email,
            '#required' => true,
            '#attributes' => array('placeholder' => t('Enter your email'), 'style' => 'width:99%;margin-bottom:1%;'),
        ];

        $form['mo_azure_ad_trial_method'] = [
            '#type' => 'select',
            '#title' => t('Trial Method'),
            '#attributes' => array('style' => 'width:99%;height:30px;margin-bottom:1%;'),
            '#options' => [
                'Drupal to Azure CRUD operations' => t('Drupal to Azure Sync (Complete CRUD operations)'),
                'Azure to Drupal Sync' => t('Azure to Drupal Sync (Complete CRUD operations)'),
                'Not Sure' => t('Not Sure (We will assist you further)'),
            ],
        ];

        $form['mo_azure_ad_trial_description'] = [
            '#type' => 'textarea',
            '#rows' => 4,
            '#required' => true,
            '#title' => t('Description'),
            '#attributes' => array('placeholder' => t('Describe your use case here!'), 'style' => 'width:99%;'),
        ];

        $form['mo_azure_ad_trial_note'] = [
            '#markup' => t('<div>If you are not sure what to choose, you can get in touch with us on <a href="mailto:'.moUserProvisioningConstants::SUPPORT_EMAIL.'">'.moUserProvisioningConstants::SUPPORT_EMAIL.'</a> and we will assist you further.</div>'),
        ];

        $form['actions'] = ['#type' => 'actions'];

        $form['actions']['send'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
            '#attributes' => [
                'class' => [
                    'use-ajax',
                    'button--primary'
                ],
            ],
            '#ajax' => [
                'callback' => [$this, 'submitModalFormAjax'],
                'event' => 'click',
            ],
        ];

        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
        return $form;
    }

    public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
        $form_values = $form_state->getValues();

        $response = new AjaxResponse();
        // If there are any form errors, AJAX replace the form.
        if ( $form_state->hasAnyErrors() ) {
            $response->addCommand(new ReplaceCommand('#modal_example_form', $form));
        } else {
            $email = $form_values['mo_azure_ad_trial_email_address'];
            $trial_method = $form_values['mo_azure_ad_trial_method'];
            $query = $form_values['mo_azure_ad_trial_description'];
            $query_type = 'Trial Request';
            $module_name = 'Azure AD Sync';
            $utilities = new MoAzureUtilities();
            $module_version = $utilities->getModuleVersion();
            $support = new moUserProvisioningSupport($email, null, $query, $query_type, $trial_method, $module_name, $module_version);
            $support_response = $support->sendSupportQuery();

            $this->messenger->addStatus(t('Support query successfully sent. We will get back to you shortly.'));
            $response->addCommand(new RedirectCommand(Url::fromRoute('azure_ad.overview')->toString()));
        }
        return $response;
    }

    public function validateForm(array &$form, FormStateInterface $form_state) { }

    public function submitForm(array &$form, FormStateInterface $form_state) { }

}
