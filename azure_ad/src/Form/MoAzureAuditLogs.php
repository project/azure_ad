<?php

namespace Drupal\azure_ad\Form;

use Drupal;
use Drupal\azure_ad\Helper\moAzureADHelper;
use Drupal\azure_ad\MoAzureUtilities;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user_provisioning\Form\MoAuditsAndLogs;
use Drupal\user_provisioning\Helpers\moUserProvisioningAudits;

class MoAzureAuditLogs extends FormBase
{
    private moUserProvisioningAudits $audits;
    protected $messenger;

    public function __construct()
    {
        $this->audits = new moUserProvisioningAudits();
        $this->messenger = Drupal::messenger();
    }

    /**
     * @inheritDoc
     */
    public function getFormId()
    {
        return "mo_azure_azure_audit_logs";
    }

    /**
     * @inheritDoc
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['mo_user_provisioning_add_css'] = array(
            '#attached' => array(
                'library' => array(
                    'user_provisioning/user_provisioning.admin',
                    'azure_ad/azure_ad.admin',
                    'core/drupal.dialog.ajax',
                )
            ),
        );

        $form['azure_audits_header_style'] = [
            '#markup' => t('<div class="mo_azure_header_container_step1">'),
        ];

        $form['azure_audit_logs_clear_logs'] = [
            '#type' => 'submit',
            '#value' => t('Clear logs'),
            '#limit_validation_errors' => [],
            '#submit' => ['::clearLogs'],
            '#attributes' => ['class' => ['button--danger']],
            '#prefix' => '<div class="azure_ad_audit_clear_logs">',
            '#suffix' => '</div>'
        ];

        $form['mo_azure_ad_audits_logs'] = [
            '#type' => 'fieldset',
            '#title' => t('Audits and Logs<hr>'),
        ];

        $user_prov_audit = new MoAuditsAndLogs();
        $row = $user_prov_audit->getCurrentLogs(null);

        $rows = [];
        foreach ($row as $index => $value) {
            $value = (array)$value;
            $rows[$index + 1] = [
                'User ID' => $value['uid'],
                'Username' => $value['name'],
                'Date' => date("F j, Y, g:i a", $value['created']),
                'Operation' => $value['operation'],
                'Status' => $value['status'],
            ];
        }

        $form['mo_azure_ad_audits_logs']['table'] = array(
            '#type' => 'table',
            '#header' => array('User ID', 'Username', 'Date', 'Operation', 'Status'),
            '#rows' => $rows,
            '#empty' => t('No record found.'),
            '#attributes' => ['class' => ['mo_azure_audits']]
        );

        $form['mo_azure_ad_audits_logs']['pager'] = array(
            '#type' => 'pager',
        );

        $form['azure_audits_header_end'] = [
            '#markup' => t('</div>')
        ];

        (new moAzureADHelper())->moAzureShowCustomerSupportIcon($form, $form_state);
        return $form;
    }

    /**
     * @inheritDoc
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // TODO: Implement submitForm() method.
    }

    function clearLogs()
    {
        //TODO Future scope: Give an option to filter any specific type of log.
        $this->audits->clearTable();
        $this->messenger->addMessage($this->t('Logs has been cleared successfully.'));
    }
}
