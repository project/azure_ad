<?php

namespace Drupal\azure_ad\Form;

use Drupal\azure_ad\Helper\moAzureADHelper;
use Drupal\azure_ad\moAzureConstants;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\user_provisioning\moUserProvisioningConstants;

class MoAzureUpgradePlans extends FormBase
{
    /**
     * @inheritDoc
     */
    public function getFormId()
    {
        return "mo_azure_upgrade_plans";
    }

    /**
     * @inheritDoc
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['markup_library'] = array(
            '#attached' => array(
                'library' => array(
                    "user_provisioning/user_provisioning.admin",
                    'azure_ad/azure_ad.admin',
                    'core/drupal.dialog.ajax',
                )
            ),
        );

        global $base_url;
        $referer = \Drupal::request()->headers->get('referer');
        $referer = isset($referer) && !empty($referer) ? $referer : $base_url . '/azure_ad/overview';

        $form['mo_user_provisioning_back_button'] = array(
            '#type' => 'link',
            '#title' => t('&#11164; &nbsp;Back to Previous tab'),
            '#url' => Url::fromUri($referer),
            '#attributes' => ['class' => ['button', 'button--danger']],
        );

        $form['mo_user_provisioning_header'] = array(
            '#markup' => '<div><div><br><div class="container-inline mo_user_provisioning_upgrade_background_note"><h1>&nbsp; UPGRADE PLANS </h1></div><br><br>'
        );

        $features = [
            [Markup::create(t('<br><h1>FREE</h1><p class="user_provisioning_pricing_rate"><sup>$</sup> 0</p><a class="button" disabled>You are on this Plan</a><br><br>')), Markup::create(t('<br><h1>PREMIUM</h1><p class="user_provisioning_pricing_rate"><sup>$</sup> 599 <sup>*</sup></p> <a class="button button--primary" target="_blank" href="mailto:' . moUserProvisioningConstants::SUPPORT_EMAIL . moAzureConstants::SUPPORT_QUERY_CONTENT.'">&#9993; Contact Us</a> <br><br>')),],
            [Markup::create(t('<h4>FEATURE LIST</h4>')), Markup::create(t('<h4>FEATURE LIST</h4>')),],
            [
                //Features of Free version

                Markup::create(t(
                    '<div class="mo_user_provisioning_feature_list">
                            <ul class="checkmark">
                                <li>Automatic / Real Time provisioning for Creating Users from Drupal to Azure AD</li>
                                <li>Manual / On-demand provisioning for Creating Users from Drupal to Azure AD</li>
                                <li>Audits and Logs</li>
                            </ul>
                           </div>'
                )),

                //Features of Premium version
                Markup::create(t(
                    '<br><h3>ALL THE FEATURES OF FREE </h3><h2> + </h2> <br>
                           <div class="mo_user_provisioning_feature_list">
                            <ul class="checkmark">
                                <li>Automatic / Real Time Provisioning for all CRUD operations from Drupal to Azure AD</li>
                                <li>Manual / On-demand provisioning for all CRUD operations from Drupal to Azure AD</li>
                                <li>Scheduler / Cron based Provisioning for all CRUD operations from Drupal to Azure AD</li>
                                <li>Basic and Advanced User Attribute Mapping</li>
                                <li>Profile Picture Sync from Azure AD to Drupal</li>
                                <li>Manual / On-demand provisioning for all CRUD operations from Azure AD to Drupal</li>
                                <li>Scheduler / Cron based Provisioning for all CRUD operations from Azure AD to Drupal</li>
                            </ul>
                           </div>'
                )),
            ]
        ];

        $form['miniorange_oauth_login_feature_list'] = array(
            '#type' => 'table',
            '#responsive' => TRUE,
            '#rows' => $features,
            '#size' => 3,
            '#attributes' => ['class' => ['mo_upgrade_plans_features mo_user_prov_feature_table']],
        );

        $form['mo_user_provisioning_instance_note'] = array(
            '#type' => 'fieldset',
            '#prefix' => '<br>',
        );

        $form['mo_user_provisioning_instance_note']['miniorange_oauth_client_instance_based'] = array(
            '#markup' => t('<div class="mo_instance_note"><b>*</b> This module follows an <b>Instance and Subscription Based</b> licensing structure. The listed prices are for purchase of a single instance. If you are planning to use the module on multiple instances, you can check out the bulk purchase discount on our website.</div><br>
                        <div class="mo_user_provisioning_highlight_background"><b><u>What is an Instance:</u></b> A Drupal instance refers to a single installation of a Drupal site. It refers to each individual website where the module is active. In the case of multisite/subsite Drupal setup, each site with a separate database will be counted as a single instance. For eg. If you have the dev-staging-prod type of environment then you will require 3 licenses of the module (with additional discounts applicable on pre-production environments).</div>'),
        );

        $azure_ad_helper = new moAzureADHelper();
        $azure_ad_helper->moAzureShowCustomerSupportIcon($form, $form_state);

        return $form;
    }

    /**
     * @inheritDoc
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // TODO: Implement submitForm() method.
    }
}
