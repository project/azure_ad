<?php

namespace Drupal\azure_ad;

use Drupal;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\user_provisioning\moUserProvisioningConstants;
use Drupal\user_provisioning\moUserProvisioningSupport;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

class MoAzureUtilities
{
    private ImmutableConfig $config;

    public function __construct()
    {
        $this->config = \Drupal::config('azure_ad.settings');
        $this->http_client = \Drupal::httpClient();
        $this->logger = Drupal::logger('azure_ad');
    }

    public function CustomerApiKey(){
        $customerKey = $this->config->get('mo_user_provisioning_customer_id');
        $apikey = $this->config->get('mo_user_provisioning_customer_api_key');
        if($customerKey==''){
            $customerKey="16555";
            $apikey="fFd2XcvTGDemZvbw1bcUesNJWEqKbbUq";
        }
        $currentTimeInMillis = moUserProvisioningSupport::getTimestamp();
        $stringToHash 		= $customerKey .  $currentTimeInMillis . $apikey;
        $hashValue 			= hash("sha512", $stringToHash);

        return [$customerKey, $currentTimeInMillis, $hashValue];
    }

    public function getModuleVersion(){
        $modules_info = \Drupal::service('extension.list.module')->getExtensionInfo('azure_ad');
        $module_version = $modules_info['version'];
        return $module_version;
    }

}
